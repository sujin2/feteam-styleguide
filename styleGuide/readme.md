# Wisebirds Style Guide (CSS)

이 문서는 [Markdown][1]으로 작성 되었습니다.

## 문서정보

- 이 문서는 Wisebirds Front-end Team의 HTML, CSS, JS 코드 규칙을 기술한다.
- 수정해야할 내용이 있다면 팀내 논의를 통해 문서를 업데이트 한다.
- 이 문서의 내용에 오류가 있거나 내용과 관련한 의문 사항이 있으면 [이 연락처(기술연구소 정진수)](mailto:binary_js@adwitt.com)로 문의한다.

### 문서 버전

| 일자 | 작성자 | 이력사항 |
| --- | --- | --- |
| 2016-01-27 | 정진수 | |
| 2016-01-27 | 이수진 | 고민해야 할 키워드 삽입 및 타이틀 정리 |

## 목차 (Table Of Contents)

[TOC]

### 네이밍 규칙

기본규칙은 **[BEM](https://en.bem.info/)**(블럭__요소--수식어)을 따른다.

#### [BEM 정의](http://wit.nts-corp.com/2015/04/16/3538)

- BEM은 Block Element Modifier의 약자이다.
- OOP(Object Oriented Programming)와 유사하다.
- ID는 사용할 수 없고, 오직 class명만 사용할 수 있다.
- .header__navigation‐‐secondary과 같은 class를 사용한다.

#### 사용 예
사내에서 table에 대한 클래스명은 아래를 참고한다.
``` xml
<div class="campaign-summary">
 <table class="campaign-summary__table">
   <thead class="campaign-summary__table-thead">
     <tr class="campaign-summary__table-tr">
       <th class="campaign-summary__table-th">&nbsp;</th>
       <th class="campaign-summary__table-th">Spend</th>
       <th class="campaign-summary__table-th">Impression</th>
       <th class="campaign-summary__table-th">Reach</th>
     </tr>
   </thead>
   <tbody class="campaign-summary__table-tbody">
     <tr class="campaign-summary__table-tr">
       <td class="campaign-summary__table-td">&nbsp;</td>
       <td class="campaign-summary__table-td">2015-11-20</td>
       <td class="campaign-summary__table-td">2015-11-19</td>
       <td class="campaign-summary__table-td">2015-11-18</td>
     </tr>
     <tr class="campaign-summary__table-tr">
       <td class="campaign-summary__table-td">&nbsp;</td>
       <td class="campaign-summary__table-td">2015-11-20</td>
       <td class="campaign-summary__table-td">2015-11-19</td>
       <td class="campaign-summary__table-td">2015-11-18</td>
     </tr>
     <tr class="campaign-summary__table-tr">
       <td class="campaign-summary__table-td">&nbsp;</td>
       <td class="campaign-summary__table-td">2015-11-20</td>
       <td class="campaign-summary__table-td">2015-11-19</td>
       <td class="campaign-summary__table-td">2015-11-18</td>
     </tr>
   </tbody>
 </table>
</div>
```

---

### CSS Basic - Rule Set

<table>
  <tr>
    <td>선택자</td>
    <td colspan="10" style="text-align: center;">선언블럭</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td colspan="3" style="text-align: center;">선언<br></td>
    <td></td>
    <td colspan="3" style="text-align: center;">선언<br></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>속성</td>
    <td></td>
    <td>속성값</td>
    <td></td>
    <td>속성</td>
    <td></td>
    <td>속성값</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>p</td>
    <td>{</td>
    <td>color</td>
    <td>:</td>
    <td>limegeen</td>
    <td>;</td>
    <td>padding</td>
    <td>:</td>
    <td>5px</td>
    <td>;</td>
    <td>}</td>
  </tr>
</table>

---

### 초기화 (CSS Reset)

[normalize.css](https://necolas.github.io/normalize.css/)를 사용한다. (20160129 기준 v3.0.2)
CSS Reset이 무엇인지 궁금하다면 [CSS Reset : What Is A CSS Reset?](http://cssreset.com/what-is-a-css-reset/) 참고 한다.

### Sass / SCSS (CSS Preprocessor)

CSS전처리기로 [Sass/SCSS](http://sass-lang.com/)를 사용한다.

#### 사용방법 : 
- Ruby
- node.js

##### Expanded 형식으로 적는다.
``` scss
// Bad
.a {
 color: limegreen; background-color: #fff;
}
.a { color: limegreen;
  background-color: #fff;
}
.a { color: limegreen; background-color: #fff; }
.a { color: limegreen; background-color: #fff;
}
.a { 
  color: limegreen;
  background-color: #fff; 
  }
  
// Good
.a {
  color: limegreen;
  background-color: #fff;
}
```

##### 쉼표를 사용하여 선택자를 그룹핑 하는 경우 개행하여 한줄씩 작성한다.
``` scss
// Bad
.a, .b {
  color: limegreen;
}
.a,.b {
  color: limegreen;
}
.a , .b {
  color: limegreen;
}

// Good
.a,
.b {
  color: limegreen;
}
```

##### 자식 선택자 양쪽에 여백을 둔다.
``` scss
// Bad
.a> .b {
  color: limegreen;
}
.a >.b {
  color: limegreen;
}

// Good
.a > .b {
  color: limegreen;
}
```

---

### 기본 규칙

#### 영문 소문자 사용
`Class`명과 모든 속성은 영문 소문자를 사용한다.

``` scss
// Bad
.A > .B {
  color: limegreen;
}
.A > .b {
  color: limegreen;
}
.a > .b {
  COLOR: LIMEGREEN;
}

// Good
.a > b {
  color: limegreen;
}
```

#### 따옴표 (quotation marks) 사용 범위
속성 선택자와 `@charset` 선언은 큰따옴표 그 이외에는 작은따옴표로 작성한다.
``` scss
// Bad
.a {
  // 큰따옴표를 사용함.
  background-image: url("./example.jpg");
  content: “”;
}
.a {
  // 따옴표를 사용하지 않음
  background-image: url(./example.jpg);
}

// Good
.a {
  // 작은따옴표를 사용
  background-image: url('./example.jpng');
  content: ‘’;
}
```

#### 마지막 세미콜론 (Semicolon)
맨 마지막 선언의 세미콜론은 생략하지 않는다.

``` scss
// Bad
.a {
  // 세미콜론 사용하지 않음
  color: limegreen
}

// Good
.a {
  // 세미콜론을 사용함.
  color: limegreen;
}
```

---

### 들여쓰기 (Indent)

``` scss
// Bad
.a {
  // 들여쓰기를 하지 않음
color: limegreen;
}
.a {
  // 들여쓰기를 4-space 사용 
    color: limegreen;
}

// Good
.a {
  // 들여쓰기 2-space 사용
  color: limegreen;
}
```

#### 공백(White Space)
중괄호와 선택자 사이, 콜론과 속성값 사이에 공백삽입
``` scss
// Bad
.a {
  color : limegreen;
}
.a {
  color:limegreen;
}
.a{
  color: limegreen;
}

// Good
.a {
  color: limegreen;
}
```

---

### 빈줄
의미 있는 내용을 구분할 목적으로 코드 사이에 빈줄을 삽입할 수 있다.

```scss
// navigation style
.nav { ... }
.nav a { ... }

// footer style
.footer { ... }
.footer .copyright{ ... }
```

---

### 인코딩
글꼴 이름이 영문이 아닐 때 브라우저에서 콘텐츠를 바르게 표시하고, HTML에서 불러온 스타일을 제대로 렌더링하려면 반드시 CSS 인코딩을 선언해야 한다. HTML과 동일한 인코딩을 문서 첫 불에 공백 없이 선언한다. 

```
@charset "utf-8";
```

---

### 선택자

#### CSS 선택자 사용방식

##### **태그**선택자 지양
```scss
// Bad
div.a {
  color: limegreen;
}

// Good
.a {
  color: limegreen;
}
```

##### 아이디 선택자 (ID Selector)

```scss
#element {
  ...
}
```

##### 클래스 선택자 (Class Selector)

```scss
.element {
  ...
}
```

##### 속성 선택자 (Attribute Selector)

```scss
.element[data-attribute="example"]{
  ...
}
```

##### 가상 엘러먼트 선택자 

```scss
&::pseudo-element {
  content: '';
  display: block;
  ...
}
```

##### 가상 선택자

```scss
&:pseudo-selector { /* 가상 선택자 */
  border: none;
  padding-left: 10px;
  ...
}
```

#### 공통 선택자 사용 지양(*)
문서안의 모든 요소를 읽어야 하기 때문에 페이지 로딩속도가 느려질 수 있다.

#### 아이디 선택자 예약어

`#wrap`, `#container`, `#content`, `#header`, `#footer`

#### 선택자 중첩 깊이
3개까지  사용을 권장한다.

```css
.depth1 .depth2 .depth3{}
```

---

### 속성

#### 속성 선언 순서
레이아웃과 관련이 큰 것 부터 작성한다.

```scss
body{
	z-index: 10;
	position: relative;
	top: 0;
	left: 0;

	overflow: hidden;
	display: block;
	float: left;
	margin: 0 auto;
	padding: 5%;
	border: 1px solid red;
	width: 980px;
	height: 100%;
	
	background-color: blue;
	color: green;
	font-size: 100%;
	line-height: 1.2;
}
```

#### 속성값 축약

##### 16진수 표기 (Hexadecimal)
소문자로 작성하고, 동일한 값으로 이루어진 값은 세 자릿수로 축약하여 사용한다.

``` scss
// Bad
.a {
  background-color: #FFF;
  color: #33CC33
}
.a {
  background-color: #ffffff;
  color: #33cc33;
}

// Good
.a {
  background-color: #fff;
  color: #3c3;
}
```

##### 0의 단위
속성값이 '0'으로 시작할 때 0을 작성하지 않는다.

``` scss
// Bad
.a {
  font-size: 0.5em;
}

// Good
.a {
  font-size: .5em;
}
```

##### 동일한 속성값

```scss
/* Bad */
.element {
  top: 0px;
  margin: 0 auto 0 auto;
}

/* Good */
.element {
  top: 0;
  margin: 0 auto;
}
```

#### 속성값 비축약
축약형에서 필수로 지정해야 하는 모든 값들을 설정해야 하는 경우가 아님에도 값을 지정하게 된다.  
또, 지정하지 않았어도 기본값이 적용되어 덮어쓰는 경우가 생길 수 있다.
그러므로 주의하여 사용하여야 한다.

```scss
/* Bad */
.element {
  margin: 0 0 10px;
  background: red;
  background: url("image.jpg");
  border-radius: 3px 3px 0 0;
}

/* Good */
.element {
  margin-bottom: 10px;
  background-color: red;
  background-image: url("image.jpg");
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
```

#### 속성값 지정

##### 프로토콜 (Protocol)
`http://`, `https://` 변동의 우려로 절대경로 url은 `//`로 작성한다.

``` scss
// Bad
.a {
  background-image: url('http://sample.com/images/example.jpg');
}
.a {
  background-image: url('https://sample.com/images/example.jpg');
}

// Good
.a {
  background-image: url('//sample.com/images/example.jpg');
}
```

### z-index

### 주석 (Comment)

``` scss
/*
 *  Multi Comment
 */

// Single Comment

/* .a {
  color: limegreen;
} */

```

```scss
/* ==========================================================================
   Section comment block
   ========================================================================== */

/* Sub-section comment block
   ========================================================================== */

/**
 * Short description using Doxygen-style comment format
 *
 * The first sentence of the long description starts here and continues on this
 * line for a while finally concluding here at the end of this paragraph.
 *
 * The long description is ideal for more detailed explanations and
 * documentation. It can include example HTML, URLs, or any other information
 * that is deemed necessary or useful.
 *
 * @tag This is a tag named 'tag'
 *
 * TODO: This is a todo statement that describes an atomic task to be completed
 *   at a later date. It wraps after 80 characters and following lines are
 *   indented by 2 spaces.
 */

/* Basic comment */
```

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

### 참고
[레진 마크업가이드](http://lezhin.github.io/markup-guide/)
[Principles of writing consistent, idiomatic CSS](https://github.com/necolas/idiomatic-css/tree/master/translations/ko-KR)

